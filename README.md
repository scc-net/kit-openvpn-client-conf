# kit-openvpn-client-conf

This repository contains all client configuration files for the [KIT OpenVPN](https://www.scc.kit.edu/dienste/openvpn.php) service.

We recommend to upgrade to the latest OpenVPN client release of version 2.6. Nevertheless, the latest minor releases of versions 2.4 and 2.5 will work as well.

KIT OpenVPN offers L3 (standard) VPN access as well as L2 (vpn2vlan). These distinct configuration settings are available for both layers (defaults in **bold**):

* Tunnel endpoint IP protocol: IPv6 or IPv4
* Split: **non-split** or split tunneling
* Protocol: **UDP** or TCP
* Port: **1194** or 443
* MTU: **openvpn default** or lower

OpenVPN allows to specify multiple remote servers in the configuration file. A client will attempt to connect to one at a time until a connection succeeds. While this feature is supported on Windows, Android, iOS, macOS Tunnelblick, and the Linux command line, it is unfortunately not available yet in Viscosity and Linux Network Manager.

All configuration files are maintained in this git repository. The version number can be found in the header of every configuration file.
It is composed of the server-side OpenVPN version, a hyphen and a number which is incremented on every configuration change (e.g. 2.6-2).
Even if only parts of the configuration files are changed the number is always increased in every file.

Non-default or non-obvious configuration settings are explained inside every config file.

On the server, the following data ciphers are allowed to be negotiated: AES-256-GCM, CHACHA20-POLY1305
If you wish to restrict the allowed ciphers for your client, configure one of the following lines:
```
data-ciphers CHACHA20-POLY1305
data-ciphers AES-256-GCM
```

Please note that we could change the server side allowed data-ciphers at any time without notice. But these changes should be rare and only occur for security reasons.
